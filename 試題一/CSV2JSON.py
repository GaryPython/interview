import csv
import json
import os

def csv_to_json(INPUT_PATH,FILE_NAME):
    data = []
    #設定路徑位置、檔案名稱
    print('路徑位置：{}'.format(INPUT_PATH))
    print('檔案名稱：{}'.format(FILE_NAME))
    
    #組合成完整檔案路徑
    PATH = INPUT_PATH+'/'+FILE_NAME
    
    #開啟CSV檔案,指定編碼 utf-8-sig
    with open(PATH, 'r', encoding='utf-8-sig') as file:
    
    #CSV檔案轉字典格式
        reader = csv.DictReader(file)
    #寫回圈，判斷欄位  member_id, tag_name是否需要放在同一個欄位 
        for row in reader:
            #每一個欄位都放在一個變數中進行判斷
            member_id = row['member_id']
            tag_name = row['tag_name']
            detail_name = row['detail_name']
            detail_value = row['detail_value']
            
            # 檢查member_id是否已存在於 data 中，若是存在，放入tags中
            existing_data = next((item for item in data if item['_id'] == member_id), None)
            if existing_data:
                #選擇tags的list，檢查tag_name否已存在於 tag 中
                existing_tags = existing_data['tags']
                existing_tag = next((tag for tag in existing_tags if tag['tag_name'] == tag_name), None)
                #若是存在，放入detail中
                if existing_tag:
                    existing_detail = {'detail_name': detail_name, 'detail_value': detail_value}
                    existing_tag['detail'].append(existing_detail)
                #若是不存在，放入tag
                else:
                    new_tag = {'tag_name': tag_name, 'detail': [{'detail_name': detail_name, 'detail_value': detail_value}]}
                    existing_tags.append(new_tag)
            # 檢查member_id是否已存在於 data 中，若是不存，新增一個完整的字典
            else:
                new_data = {'_id': member_id, 'member_id': member_id, 'tags': [{'tag_name': tag_name, 'detail': [{'detail_name': detail_name, 'detail_value': detail_value}]}]}
                data.append(new_data)
    #輸出方式(一)：執行後可以直接print結果
    #串列轉換JSON 字符
    json_data = json.dumps(data, indent=4, ensure_ascii=False)
    #確認格式轉換成功
    print('完成CSV至JSON格式轉換')
    
    #輸出方式(二)：執行後會出轉換結果
    #完成JSON格式輸出
    OUTPUT_PATH = INPUT_PATH+'/CSV2JSON.json'
    with open(OUTPUT_PATH, 'w', encoding='utf-8') as file:
        json.dump(data, file, indent=4, ensure_ascii=False)
        print('完成JSON格式輸出')

    return json_data




#確認執行轉換CSV檔案位置
#默認設定位 CSV2JSON.py 所在路徑位置
INPUT_PATH = os.path.dirname(os.path.abspath(__file__))
#選擇檔案名稱
FILE_NAME = 'CSV2JSON.csv'
#執行函式
csv_to_json(INPUT_PATH,FILE_NAME)
#可以將轉換JSON 結果print 出來
#display = csv_to_json(INPUT_PATH,FILE_NAME)
#print(display) 
