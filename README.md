# 程式執行說明



## 試題1
1. 進入 Gitlab-> interview-> 試題一
2. 下載 Zip 壓縮檔
3. 開啟 CSV2JSON.py(建議 Visual Studio Code)
4. 移動第 64 行開始設定輸入路徑、檔案名稱
5. 輸入路徑:INPUT_PATH(默認 CSV2JSON.py 所在路徑位置)
6. 輸入檔案名稱:FILE_NAME
7. 放入函式參數中
8. 執行函式
### 題目
【資料科學工程考題_賴巖鑫.pdf】

### 說明書
【CSV2JSON說明書】
>(一)檔案說明

>(二)程式邏輯

>(三)使用步驟

>(三)結果測試



### 資料格式轉換程式
【CSV2JSON.py】
### 輸入CSV
【CSV2JSON.csv】
### 輸出JSON 結果
【CSV2JSON.json】
